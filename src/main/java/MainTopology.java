import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

public class MainTopology {
    private static final Logger logger = LoggerFactory.getLogger(MainTopology.class);
    public static void main(String[] args) {

        String topic = "test";
        String groupId = "consumer-storm";
        String zkRoot = "/kafkastorm";
        String brokerZkPath = "/brokers";
        String boltParallelism = "1";
        String spoutParallelism = "1";
        int worker = 1;
        String topologyName = "storm-topology";

        ZkHosts zkHost = new ZkHosts("localhost:2181",brokerZkPath);
        SpoutConfig spoutconfig = new SpoutConfig(zkHost,topic,zkRoot,groupId);
        spoutconfig.fetchSizeBytes = 2097152;
        spoutconfig.startOffsetTime = kafka.api.OffsetRequest.LatestTime();

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("spout1",new KafkaSpout(spoutconfig),Integer.parseInt(spoutParallelism));
        builder.setBolt("bolt1",new Bolt1(),Integer.parseInt(boltParallelism)).shuffleGrouping("spout1");

        Config conf = new Config();
        conf.setDebug(false);
        conf.setNumWorkers(worker);
        conf.setMaxSpoutPending(500);

        conf.put("environment","local");
        conf.put("consumer-id",groupId);
        conf.put("topic",topic);
        conf.put("last-deployed",System.currentTimeMillis());
        conf.put("zk-root",zkRoot);
        conf.put("start_time",System.currentTimeMillis());

        try {
            StormSubmitter.submitTopologyWithProgressBar(topologyName,conf, builder.createTopology());
        } catch (Exception e) {
            System.out.println("Storm Submitter Failed : " + e.toString());
            logger.debug("Storm Submitter Failed : ",e);

        }
    }

}
