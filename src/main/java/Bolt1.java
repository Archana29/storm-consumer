import com.springboot.kafkaproducer.Bookings;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Bolt1 extends BaseBasicBolt {
    private static Logger logger = LoggerFactory.getLogger(Bolt1.class);

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        try {
            Bookings.CabServices message = Bookings.CabServices.parseFrom(input.getBinary(0));
            csvFile(message);
            collector.emit(new Values(message.toByteArray()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void csvFile(Bookings.CabServices message) throws IOException {
        try{
            File file = new File("/home/subbarapuarchana/Downloads/demo-storm/storm1/output1.csv");
            FileWriter fr = new FileWriter(file,true);
            fr.write(message.getRegVehicle(0).getVehicleNum()+",");
            fr.write(message.getRegVehicle(0).getModel()+",");
            fr.write(String.valueOf(message.getRegVehicle(0).getVtype())+",");
            fr.write(message.getRegVehicle(0).getDriver1().getName()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getLicense()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getId()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getPhone()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getEmail()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getAdd().getAddressline1()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getAdd().getAddressline2()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getAdd().getCity()+",");
            fr.write(message.getRegVehicle(0).getDriver1().getAdd().getState()+",");
            fr.write("\n");
            fr.close();
        }
        catch (Exception e){
            logger.info("write failed with exception  " + e);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}




